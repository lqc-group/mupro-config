TARGET = Ferroelectric.exe
IFC = mpiifort

INTELSTATICFFTWROOT=/gpfs/group/lqc3/cyberstar/fftw

MKLROOT=/opt/intel/compilers_and_libraries_2016.3.210/linux/mkl
MUPROROOT=$(shell pwd)/../../Library

INTELSTATICFFTWINC = -I${INTELSTATICFFTWROOT}/include/
INTELSTATICFFTWLIB = ${INTELSTATICFFTWROOT}/lib/libfftw3_mpi.a ${INTELSTATICFFTWROOT}/lib/libfftw3.a

# IFCFLAGS= -O0 -xSSE3 -align rec16byte -align array64byte -align all -r8 -traceback -g

IFCFLAGS= -O3 -xSSE3 -align rec16byte -align array64byte -align all -r8 -traceback -g


FC=$(IFC)
FCFLAGS=$(IFCFLAGS)
FFTWINC = $(INTELSTATICFFTWINC)
FFTWLIB = ${INTELSTATICFFTWLIB}
MKLLIB = ${MKLROOT}/lib/intel64/libmkl_lapack95_ilp64.a ${MKLROOT}/lib/intel64/libmkl_scalapack_ilp64.a -Wl,--start-group ${MKLROOT}/lib/intel64/libmkl_intel_lp64.a ${MKLROOT}/lib/intel64/libmkl_sequential.a   ${MKLROOT}/lib/intel64/libmkl_core.a ${MKLROOT}/lib/intel64/libmkl_blacs_intelmpi_ilp64.a -Wl,--end-group -lpthread -liomp5 -lm -ldl #-Wl,-rpath=XORIGIN/lib
MUPROLIB = ${MUPROROOT}/libpfMPI.a

MUPROINC = -I${MUPROROOT}
MKLINC = -I${MKLROOT}/include

SCR_F = Ferro_modules.f90 Ferroelectric.f90

OBJS = $(SCR_F:.f90=.o) $(SCR_C:.c=.o)

${TARGET}: $(OBJS)
	$(FC) $(FCFLAGS) -o $@ $(OBJS) $(MUPROLIB) $(FFTWLIB) $(MKLLIB) -L/usr/global/curl/7.49.1/lib -lcurl -lstdc++

%.o: %.f90
	$(FC) $(FCFLAGS) $(MUPROINC) $(MKLINC) -c $<


.PHONY:clean
clean:
	/bin/rm -f *.o
	/bin/rm -f *.mod
	/bin/rm -f *.a
