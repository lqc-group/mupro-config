IFC=mpiifort
ICC=mpiicc
ICPC=mpiicpc

INTELSTATICFFTWROOT=/gpfs/group/lqc3/cyberstar/fftw
INTELSTATICFFTWINC = -I${INTELSTATICFFTWROOT}/include/

# IFCFLAGS= -O0 -xSSE3  -align rec16byte -align array64byte -align all -g -traceback
# ICCFLAGS= -O0 -g -traceback -C
IFCFLAGS= -O3 -xSSE3 -align rec16byte -align array64byte -align all -g -traceback
ICCFLAGS= -O3

CC=$(ICC)
FC=$(IFC)
FCFLAGS=$(IFCFLAGS)
CCFLAGS=$(ICCFLAGS)
FFTWINC=$(INTELSTATICFFTWINC)

SCR_F := func_modules.f90 print_utilities.f90 fftw_mpi.f90 mpi_output.f90 utilities.f90\
        error_handling.f90 rotate_routines.f90 \
        msolve.f90 cubic_msolve.f90 mpi_utilities.f90 diffusion.f90 \
	    elastic_routines.f90 polyElastic_routines.f90 \
	    electric_routines.f90 polyElectric_routines.f90 \
		flexo_routines.f90 ferroelectric_routines.f90\
	    magnetic_routines.f90 polyMagnetic_routines.f90 \
		structural2D.f90 defect_routines.f90 oxygenTilt_routines.f90\
	    functions_cheb.f90 electric_cheb.f90 diffusion_species_cheb.f90 \
	    current_cheb.f90 landau_cheb.f90 elasticity_cheb.f90 polarization_cheb.f90 \
		tdgl_lapack_routines.f90 solvers.f90 structure_construction.f90\
		fson_string_m.f90 fson_value_m.f90 fson_path_m.f90 fson.f90 \
		domain_processing.f90  electric_fdm_2D.f90 \
		ferroelectric_routines.f90 json_parser.f90\
		license_check.f90 free_format.f90 #read_potential.f90


# SCR_F := $(wildcard *.f90)
SCR_FSON := fson_string_m.f90 fson_value_m.f90 fson_path_m.f90 fson.f90
# SCR_NO_FSON := $(filter-out fson_string_m.f90 fson_value_m.f90 fson_path_m.f90 fson.f90,$SCR_F)

SCR_C = fftw_workhorse.c
SCR_CPP = license.cpp exprtk.cpp parseExpression.cpp
SCR_HPP =

SCR_M = mod_fftw_mpi.mod mod_interfaces.mod fson.mod fson_path_m.mod fson_value_m.mod fson_string_m.mod postprocessing.mod double_precision.mod free_format.mod print_utilities.mod surfaceelecfdm.mod json_parser.mod

OBJS =  $(SCR_F:.f90=.o) $(SCR_C:.c=.o) $(SCR_CPP:.cpp=.o) $(SCR_HPP:.hpp=.o)



libpfMPI.a: $(OBJS)
	ar rcs $@ $(OBJS) $(SCR_M)
	ar rcs structure.a structure.mod structure_construction.o
	ar rcs free_format.a free_format.mod free_format.o
	ar rcs json_parser.a json_parser.mod json_parser.o fson.mod fson_value_m.mod fson_string_m.mod fson_path_m.mod fson.o fson_value_m.o fson_path_m.o fson_string_m.o
	ar rcs print_utilities.a print_utilities.mod print_utilities.o

# mpi_output.o : mpi_output.f90
	# $(FC) $(FCFLAGS) -c mpi_output.f90

# mpi_utilities.o : mpi_utilities.f90
	# $(FC) $(FCFLAGS) -c mpi_utilities.f90

fso%.o: fso%.f90
	$(FC) $(FCFLAGS) -c $<

%.o: %.f90
	$(FC) $(FCFLAGS) -r8 -c $<

%.o: %.c
	$(CC) $(CCFLAGS) ${FFTWINC} -I/usr/global/curl/7.49.1/include -c $<

%.o: %.cpp
	$(ICPC) $(CCFLAGS) -c $<

%.o: %.hpp
	$(CC) $(CCFLAGS) -c $<


.PHONY:clean
clean:
	/bin/rm -f *.o
	/bin/rm -f *.mod
	/bin/rm -f *.a
