TARGET = EffProperty.exe
IFC = mpiifort

INTELSTATICFFTWROOT=/gpfs/group/lqc3/cyberstar/fftw

MKLROOT=/opt/intel/compilers_and_libraries_2016.3.210/linux/mkl
MUPROROOT=$(shell pwd)/../../Library

INTELSTATICFFTWINC = -I${INTELSTATICFFTWROOT}/include/
INTELSTATICFFTWLIB = ${INTELSTATICFFTWROOT}/lib/libfftw3_mpi.a ${INTELSTATICFFTWROOT}/lib/libfftw3.a

# IFCFLAGS= -O0 -xSSE3 -C -warn -align rec16byte -align array64byte -align all -r8 -traceback -g
IFCFLAGS= -O3 -xSSE3 -align rec16byte -align array64byte -align all -r8

FC=$(IFC)
FCFLAGS=$(IFCFLAGS)
FFTWINC = $(INTELSTATICFFTWINC)
FFTWLIB = ${INTELSTATICFFTWLIB}
MKLLIB = ${MKLROOT}/lib/intel64/libmkl_lapack95_ilp64.a -Wl,--start-group ${MKLROOT}/lib/intel64/libmkl_gf_lp64.a ${MKLROOT}/lib/intel64/libmkl_sequential.a   ${MKLROOT}/lib/intel64/libmkl_core.a -Wl,--end-group -lpthread -lm -ldl #-Wl,-rpath=XORIGIN/lib
MUPROLIB = ${MUPROROOT}/libpfMPI.a

MUPROINC = -I${MUPROROOT}
MKLINC = -I${MKLROOT}/include

SCR_F = EffProperty.f90

OBJS = $(SCR_F:.f90=.o)

${TARGET}: $(OBJS)
	$(FC) $(FCFLAGS) -o $@ $(OBJS) $(MUPROLIB) $(FFTWLIB) $(MKLLIB) -L/usr/global/curl/7.49.1/lib -lcurl -lstdc++

%.o: %.f90
	$(FC) $(FCFLAGS) $(MKLINC) $(MUPROINC) -c $<


clean:
	/bin/rm -f *.o *.mod *.exe *.dat fort.* *pbs.*
###
