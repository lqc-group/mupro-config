# mupro-docker

The repository contains different dockerfile and docker-compose.yml that we've been using for creating the development and release environment for MUPRO, the phase field simulation package. It also comes with example Makefile or CMakeLists.txt file.

# Contents
| Base Image | Version | MPI | FFTW | MKL | curl | ssl | VTK | HDF5 |                  
|------------|---------|-----|------|-----|------|-----|-----|------|
| Alpine     | 3.7     | `OPENMPI` | `FFTW` | `BLAS+LAPACK` | `CURL` | openssl-dev | `VTK` | hdf5-dev     |
| Ubuntu     | 18.04   | mpich| `FFTW` | intel-mkl | `CURL` | libssl-dev | `VTK` | libhdf5-dev |
| Centos     |  Working       |                                |


# Building images 
Only the Dockerfile is needed which controls the procedures of building a docker image.
## Alpine
1. Go to the Alpine folder
2. ```docker build -t 101010011001/vtk-env-alpine:1.0 .```
    * `.` means build the image based on the Dockerfile in the current directory
    * `-t` set the tag name for the built image.
3. `docker push 101010011001/vtk-env-alpine`
    * push the compiled image to the docker hub, remember to `docker login` before you push

## Ubuntu
1. Go to the Ubuntu/vtk folder
2. `docker build -f ubuntu-vtk.dockerfile -t 101010011001/vtk-ubuntu:1.0 .`
3. `docker push 101010011001/vtk-ubuntu`
4. Go to the Ubuntu/mupro folder
5. `docker build -f mupro-ubuntu.dockerfile -t 101010011001/mupro-ubuntu:1.0 .`
6. `docker push 101010011001/mupro-ubuntu`
## Centos (Not ready yet)


# Usage
When using the already built images, only the docker-compose.yml file is needed, which controls the settings when you start a container for the image. And there are three ways to start the container you want:
1. Use DockStation. DockStation is a third party software that provides you a gui to load the docker-compose.yml file and organize your projects. **PRO**, it is easy to use for beginner, **CON** you cannot change the mapped volume easily, you need to either copy the docker-compose.yml file to your desired folder or change the volume part of the file.
2. User Kitematic. Kitematic is the official gui for managing your docker containers. **PRO**, you can change the volume mapping easily, no docker-compose.yml is needed. **CON**, you won't be able to set some more advanced options such as SYS_PTRACE, which limits your capability to do debugging.
3. Use the command line. `docker run --cap_add=SYS_PTRACE --security_opt=unconfined -v LOCAL_FOLDER:FOLDER_IN_DOCKER -it --rm YOUR_IMAGE:IMAGE_TAG /bin/bash`
