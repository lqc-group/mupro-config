FROM alpine:3.8

LABEL author="Xiaoxing Cheng"
LABEL email="0001001bill@gmail.com"
LABEL description="The VTK 8.1.1 C++ setup based on alpine linux"

RUN apk update && \
    apk upgrade && \
    apk add gcc && \
    apk add g++ && \
    apk add make && \
    apk add file && \
    apk add cmake && \
    apk add gdb && \
    apk add wget && \
    apk add git && \
    apk add glew-dev && \
    apk add libxt-dev

ARG VTK_VERSION="8.1.1"
RUN mkdir /tmp/vtk-src
WORKDIR /tmp/vtk-src
RUN wget https://www.vtk.org/files/release/8.1/VTK-${VTK_VERSION}.tar.gz &&\
    tar xfz VTK-${VTK_VERSION}.tar.gz && \
    mkdir vtk-build && \
    cd vtk-build && \
    cmake \
    -D CMAKE_BUILD_TYPE:STRING=Release \
    -D CMAKE_INSTALL_PREFIX:STRING=/usr \
    -D BUILD_DOCUMENTATION:BOOL=OFF \
    -D BUILD_EXAMPLES:BOOL=OFF \
    -D BUILD_TESTING:BOOL=OFF \
    -D BUILD_SHARED_LIBS:BOOL=OFF \
    -D VTK_RENDERING_BACKEND:STRING=OpenGL2 \
    -D VTK_Group_MPI:BOOL=OFF \
    -D VTK_Group_StandAlone:BOOL=OFF \
    -D VTK_Group_Rendering:BOOL=ON \
    ../VTK-${VTK_VERSION} && \
    make && \
    make install

RUN rm -rf /tmp/*

ARG HOMEDIR=/project
ENV WORKDIR ${HOMEDIR}
RUN mkdir ${HOMEDIR}
WORKDIR ${HOMEDIR}

VOLUME /project
CMD ["/bin/ash"]
