FROM ubuntu:18.04

LABEL author="Xiaoxing Cheng"
LABEL email="0001001bill@gmail.com"
LABEL description="The ubuntu + vtk setup with necessary build utilities"

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && \
    apt-get -y upgrade && \ 
    apt-get install -y \
        apt-utils \
        wget \
        binutils \
        gcc \
        g++ \
        make \
        autoconf \
        automake \
        libtool \
        flex \
        cmake \
        libxt-dev \
        mesa-common-dev \
        freeglut3-dev \
        git \
        vim \
        sudo \
        libhdf5-dev \
        tzdata && \
    echo "America/New_York" > /etc/timezone && \
    dpkg-reconfigure -f noninteractive tzdata

ARG VTK_VERSION="8.1.1"
RUN mkdir /tmp/vtk-src
WORKDIR /tmp/vtk-src
RUN wget https://www.vtk.org/files/release/8.1/VTK-${VTK_VERSION}.tar.gz &&\
    tar xfz VTK-${VTK_VERSION}.tar.gz && \
    mkdir vtk-build && \
    cd vtk-build && \
    cmake \
    -D CMAKE_BUILD_TYPE:STRING=Release \
    -D CMAKE_INSTALL_PREFIX:STRING=/usr/local \
    -D BUILD_SHARED_LIBS=OFF \
    ../VTK-${VTK_VERSION} && \
    make && \
    make install
    
RUN rm -rf /tmp/*

ARG HOMEDIR=/home
ENV WORKDIR ${HOMEDIR}

VOLUME /home
WORKDIR ${HOMEDIR}
CMD ["bash","-i"]
