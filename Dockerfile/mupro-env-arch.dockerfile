FROM archlinux/base

LABEL author="Xiaoxing Cheng"
LABEL email="0001001bill@gmail.com"
LABEL description="The arch linux + vtk setup with necessary build utilities"

RUN pacman -Syy && \
    pacman -Sy --noconfirm wget \
        base-devel \
        vtk \
        vim \
        git \
        sudo \
        hdf5

RUN useradd builduser -m && \
    passwd -d builduser && \
    printf 'builduser ALL=(ALL) ALL\n' | tee -a /etc/sudoers 
WORKDIR /tmp
RUN git clone https://aur.archlinux.org/yay.git && \
    chmod -R 777 /tmp/yay && \
    cd yay && \
    sudo -u builduser bash -c 'makepkg -si --noconfirm'

RUN sudo -u builduser bash -c 'yay -Sy --noconfirm intel-compiler-base' &&\
    sudo -u builduser bash -c 'yay -Sy --noconfirm intel-fortran-compiler' &&\
    sudo -u builduser bash -c 'yay -Sy --noconfirm intel-mpi' &&\
    sudo -u builduser bash -c 'yay -Sy --noconfirm intel-mkl' 

ARG CURL_VERSION="7.61.1"
RUN mkdir /tmp/curl-src
WORKDIR /tmp/curl-src
RUN wget https://curl.haxx.se/download/curl-${CURL_VERSION}.tar.gz && \
    tar xfz curl-${CURL_VERSION}.tar.gz && \
    cd curl-${CURL_VERSION} && \
    ./configure --with-ssl --disable-shared && \
    make && \
    make install 

ARG FFTW_VERSION="3.3.7"
ARG FFTW_CONFIGURE_OPTIONS=" "
RUN mkdir /tmp/fftw-src
WORKDIR /tmp/fftw-src
RUN wget http://www.fftw.org/fftw-${FFTW_VERSION}.tar.gz && \
    tar xfz fftw-${FFTW_VERSION}.tar.gz && \
    cd fftw-${FFTW_VERSION} && \
#     export PATH=${PATH}:/usr/lib64/mpich-3.2/bin && \
    ./configure --enable-mpi --enable-static --enable-shared && \
    make -j && \
    make install

ARG HOMEDIR=/home
ENV WORKDIR ${HOMEDIR}

VOLUME /home
WORKDIR ${HOMEDIR}
# USER builduser
CMD ["bash","-i"]


        # gcc \
        # gcc-fortran \
        # autoconf \
        # automake \
        # libtool \
        # flex \
        # gdb \
        # openssl \
        # cmake \
                # ctags \
        # cscope \