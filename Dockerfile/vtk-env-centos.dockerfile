FROM centos:7

LABEL author="Xiaoxing Cheng"
LABEL email="0001001bill@gmail.com"
LABEL description="The centos + vtk setup with necessary build utilities"

RUN yum -y update && \
    yum -y install epel-release && \
    yum -y install wget \
        binutils-x86_64-linux-gnu \
        make \
        gcc \
        gcc-c++ \
        gcc-gfortran \
        autoconf \
        automake \
        libtool \
        flex \
        gdb \
        openssl-devel \
        cmake3 \
        cmake3-gui \
        libXt-devel \
        freeglut-devel \
        vim \
        git \
        ctags \
        cscope \
        sudo \
        hdf5-devel

ARG VTK_VERSION="8.1.1"
RUN mkdir /tmp/vtk-src
WORKDIR /tmp/vtk-src
RUN wget https://www.vtk.org/files/release/8.1/VTK-${VTK_VERSION}.tar.gz &&\
    tar xfz VTK-${VTK_VERSION}.tar.gz && \
    mkdir vtk-build && \
    cd vtk-build && \
    cmake3 \
    -D CMAKE_C_COMPILER=/usr/bin/gcc  \
    -D CMAKE_CXX_COMPILER=/usr/bin/g++  \
    -D CMAKE_MAKE_PROGRAM=/usr/bin/make \
    -D CMAKE_BUILD_TYPE:STRING=Release \
    -D CMAKE_INSTALL_PREFIX:STRING=/usr/local \
    -D BUILD_SHARED_LIBS=OFF \
    ../VTK-${VTK_VERSION} && \
    make && \
    make install
    
RUN rm -rf /tmp/*

ARG HOMEDIR=/home
ENV WORKDIR ${HOMEDIR}

VOLUME /home
WORKDIR ${HOMEDIR}
CMD ["bash","-i"]
