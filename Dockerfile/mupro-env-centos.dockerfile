FROM centos:7

LABEL author="Xiaoxing Cheng"
LABEL email="0001001bill@gmail.com"
LABEL description="The mupro environemnt based on centos"

RUN yum -y update && \
    yum -y install epel-release && \
    yum -y install wget \
        binutils-x86_64-linux-gnu \
        make \
        file \
        gcc \
        gcc-c++ \
        gcc-gfortran \
        autoconf \
        automake \
        libtool \
        flex \
        gdb \
        openssl-devel \
        cmake3 \
        cmake3-gui \
        libXt-devel \
        freeglut-devel \
        vim \
        git \
        ctags \
        cscope \
        sudo \
        hdf5-devel

RUN yum-config-manager --add-repo https://yum.repos.intel.com/mkl/setup/intel-mkl.repo && \
    rpm --import https://yum.repos.intel.com/intel-gpg-keys/GPG-PUB-KEY-INTEL-SW-PRODUCTS-2019.PUB && \
    yum -y install intel-mkl-2018.2-046

RUN yum -y install mpich-3.2 mpich-3.2-devel

ARG CURL_VERSION="7.61.1"
RUN mkdir /tmp/curl-src
WORKDIR /tmp/curl-src
RUN wget https://curl.haxx.se/download/curl-${CURL_VERSION}.tar.gz && \
    tar xfz curl-${CURL_VERSION}.tar.gz && \
    cd curl-${CURL_VERSION} && \
    ./configure --with-ssl --disable-shared && \
    make && \
    make install 

ARG FFTW_VERSION="3.3.7"
ARG FFTW_CONFIGURE_OPTIONS=" "
RUN mkdir /tmp/fftw-src
WORKDIR /tmp/fftw-src
RUN wget http://www.fftw.org/fftw-${FFTW_VERSION}.tar.gz && \
    tar xfz fftw-${FFTW_VERSION}.tar.gz && \
    cd fftw-${FFTW_VERSION} && \
    export PATH=${PATH}:/usr/lib64/mpich-3.2/bin && \
    ./configure --enable-mpi --enable-static --enable-shared && \
    make -j && \
    make install
    
RUN rm -rf /tmp/*

ARG HOMEDIR=/home
ENV WORKDIR ${HOMEDIR}

VOLUME /home
WORKDIR ${HOMEDIR}
CMD ["bash","-i"]
