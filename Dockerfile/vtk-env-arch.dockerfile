FROM archlinux/base

LABEL author="Xiaoxing Cheng"
LABEL email="0001001bill@gmail.com"
LABEL description="The arch linux + vtk setup with necessary build utilities"

RUN pacman -Syy && \
    pacman -Sy --noconfirm wget \
        base-devel \
        gcc \
        gdb \
        openssl \
        cmake \
        vtk \
        vim \
        git \
        ctags \
        cscope \
        hdf5

ARG HOMEDIR=/home
ENV WORKDIR ${HOMEDIR}

VOLUME /home
WORKDIR ${HOMEDIR}
CMD ["bash","-i"]
