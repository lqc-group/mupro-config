FROM ubuntu:18.04

LABEL author="Xiaoxing Cheng"
LABEL email="0001001bill@gmail.com"
LABEL description="The development environment for MUPRO suite based on ubuntu"

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && \
    apt-get -y upgrade && \ 
    apt-get install -y \
        apt-utils \
        wget \
        binutils \
        exuberant-ctags \
        gcc \
        g++ \
        gfortran \
        make \
        autoconf \
        automake \
        libtool \
        flex \
        gdb \
        libssl-dev \
        cmake \
        cmake-curses-gui \
        cscope \
        git \
        vim \
        sudo \
        libhdf5-dev \
        python \
        tzdata && \
    echo "America/New_York" > /etc/timezone && \
    dpkg-reconfigure -f noninteractive tzdata

RUN apt-get update && \
    apt-get install -y gnupg && \
    wget https://apt.repos.intel.com/intel-gpg-keys/GPG-PUB-KEY-INTEL-SW-PRODUCTS-2019.PUB && \
    apt-key add GPG-PUB-KEY-INTEL-SW-PRODUCTS-2019.PUB && \
    wget https://apt.repos.intel.com/setup/intelproducts.list -O /etc/apt/sources.list.d/intelproducts.list && \
    apt-get update && \
    apt-get install -y intel-mkl-64bit-2018.2-046

RUN apt-get update && \
    apt-get install -y mpich

ARG CURL_VERSION="7.61.1"
RUN mkdir /tmp/curl-src
WORKDIR /tmp/curl-src
RUN wget https://curl.haxx.se/download/curl-${CURL_VERSION}.tar.gz && \
    tar xfz curl-${CURL_VERSION}.tar.gz && \
    cd curl-${CURL_VERSION} && \
    ./configure --with-ssl --disable-shared && \
    make && \
    make install 

ARG FFTW_VERSION="3.3.7"
ARG FFTW_CONFIGURE_OPTIONS=" "
RUN mkdir /tmp/fftw-src
WORKDIR /tmp/fftw-src
RUN wget http://www.fftw.org/fftw-${FFTW_VERSION}.tar.gz && \
    tar xfz fftw-${FFTW_VERSION}.tar.gz && \
    cd fftw-${FFTW_VERSION} && \
    ./configure --enable-mpi --enable-static --enable-shared && \
    make -j && \
    make install

RUN echo "source /opt/intel/compilers_and_libraries/linux/mkl/bin/mklvars.sh intel64" >> /root/.bashrc

RUN rm -rf /tmp/*

ARG HOMEDIR=/home
ENV WORKDIR ${HOMEDIR}

VOLUME /home
WORKDIR ${HOMEDIR}
CMD ["bash","-i"]

